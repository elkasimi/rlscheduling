#include "Common.h"

#include "TaskFactory.h"
#include "RNG.h"

int TaskFactory::sMinExecutionTime = 5;
int TaskFactory::sMaxExecutionTime = 100;
int TaskFactory::sMinDeadline = 10;
int TaskFactory::sMaxDeadline = 20;/* 30; 40; 50; 60; 70; 80; 90; 100 */
int TaskFactory::sMinWidth = 7;
int TaskFactory::sMaxWidth = 45;
int TaskFactory::sMinHeight = 7;
int TaskFactory::sMaxHeight = 45;

Task* TaskFactory::Create(int inTaskArrivalTime) {
	int executionTime = RNG::RandomInt(sMinExecutionTime, sMaxExecutionTime);
	int deadline = inTaskArrivalTime + executionTime + RNG::RandomInt(sMinDeadline, sMaxDeadline);
	int width = RNG::RandomInt(sMinWidth, sMaxWidth);
	int height = RNG::RandomInt(sMinHeight, sMaxHeight);
	Task* task = new Task(inTaskArrivalTime, executionTime, deadline, width, height);

	return task;
}
