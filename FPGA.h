#ifndef __FPGA__
#define __FPGA__

class Task;
class StartingTimeMatrix;

class FPGA {
public:
	FPGA(int inWidth, int inHeight);
	~FPGA();

	int GetWidth() const { return fWidth; }
	int GetHeight() const { return fHeight; }
	void Add(Task* inScheduledTask);
	void Update(int inCurrentTime);
	bool IsDone() const;
	void BuildStartingTimeMatrix(const Task* inArrivingTask, StartingTimeMatrix& outStartingTimeMatrix, int& outSmallestStartingTime) const;
	double GetTotalCompactionValueWithBoundary(const Task* inArrivingTask, int inX, int inY) const;
	double GetTotalCompactionValueWithScheduledTasks(const Task* inArrivingTask, int inX, int inY) const;
	double GetTotalFinishingTimeDifference(const Task* inArrivingTask, int inX, int inY) const;
	double GetTotalHidingValue(const Task* inArrivingTask, int inX, int inY) const;

	friend ostream& operator << (ostream& out, const FPGA& inFPGA);
	
private:
	double GetCompactionValue(const Task* inArrivingTask, int inX, int inY, const Task* inScheduledTask) const;
	double GetFinishingTimeDifference(const Task* inArrivingTask, int inX, int inY, const Task* inScheduledTask) const;
	double GetHidingValue(const Task* inArrivingTask, int inX, int inY, const Task* inScheduledTask) const;

	int fWidth;
	int fHeight;
	list<Task*> fRunningTasks;
	list<Task*> fWaitingTasks;
};

#endif
