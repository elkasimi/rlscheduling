#ifndef __TASK__
#define __TASK__

class Task {
public:
	Task(int inArrivalTime, int inExecutionTime, int inDeadline, int inWidth, int inHeight);
	
	void Update(int inCurrentTime);
	bool IsDone() const;
	int GetDeadline() const { return fDeadline; }
	int GetArrivalTime() const { return fArrivalTime; }
	int GetXPos() const { return fXPos; }
	int GetYPos() const { return fYPos; }
	int GetWidth() const { return fWidth; }
	int GetHeight() const { return fHeight; }
	int GetExecutionTime() const { return fExecutionTime; }
	int GetStartingTime() const { return fStartingTime; }
	int GetFinishingTime() const { return (fStartingTime + fExecutionTime); }

	void SetStartingTime(int inStartingTime) { fStartingTime = inStartingTime; }
	void SetXPos(int inXPos) { fXPos = inXPos; }
	void SetYPos(int inYPos) { fYPos = inYPos; }

private:
	int fArrivalTime;
	int fExecutionTime;
	int fDeadline;
	int fWidth;
	int fHeight;
	int fRunningTime;
	int fStartingTime;
	int fXPos;
	int fYPos;
};

#endif
