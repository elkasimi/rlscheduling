var scene = null;
var camera = null;
var colors = ['red', 'blue', 'gray', 'yellow', 'magenta'];

var fpgaWidth = 116;
var fpgaHeight = 192;

window.onload = function() {
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

	var renderer = new THREE.WebGLRenderer();
	renderer.setSize( window.innerWidth, window.innerHeight );
	document.body.appendChild( renderer.domElement );
	
	var addedAt = {};
	var removedAt = {};
	var c = 1;
	
	data.forEach(function(task) {
		var name = "task_" + c;
		task.name = name;
		c += 1;
		
		if(!addedAt.hasOwnProperty(task.ts)) {
			addedAt[task.ts] = [];
		}
		
		addedAt[task.ts].push(task);
		
		if(!removedAt.hasOwnProperty(task.tf)) {
			removedAt[task.tf] = [];
		}
		
		removedAt[task.tf].push(task);
	});

	camera.position.z = 5;
	camera.position.x = 200;
	
	var t = 0;
	var i = 0;
	var render = function () {
		requestAnimationFrame( render );
		
		++t;
		
		var camPos = 0;
		
		if(addedAt.hasOwnProperty(t)) {
			addedAt[t].forEach(function(task) {
				var color = colors[i];
				i = (i + 1) % colors.length;
				var geometry = new THREE.BoxGeometry( task.w, task.h, task.tf - task.ts );
				var material = new THREE.MeshBasicMaterial( { color: color } );
				var cube = new THREE.Mesh( geometry, material );
				cube.position.x = task.x;
				cube.position.y = task.y;
				cube.position.z = task.ts;
				scene.add(cube);
				
				camPos = camPos > task.tf ? camPos : task.tf;
			});
		}
		
		if(removedAt.hasOwnProperty(t)) {
			removedAt[t].forEach(function(task) {
				var cube = scene.getObjectByName(task.name);
				scene.remove( cube );			
			});
		}
		
		camera.position.z += 1;
		
		if(camera.position.z < camPos + 20) {
			camera.position.z = camPos + 20;
		}

		renderer.render(scene, camera);
	};

	render();
};
