#ifndef __SCHEDULER__
#define __SCHEDULER__

class Task;
class FPGA;

class Scheduler {
public:
	Scheduler(bool inIsStdScheduler = false);

	bool Schedule(Task* inTask, FPGA& inFPGA);
	void UpdateWeights(const double& inLastValue, const double& inCurrentValue);


	friend ostream& operator << (ostream& out, const Scheduler& inScheduler);

private:
	void NormalizeWeights();
	static double GetUpdatedValue(const double& inLastValue, const double& inDelta);

	double fTotalCompactionValueWithBoundaryWeight;
	double fTotalCompactionValueWithScheduledTasksWeight;
	double fTotalFinishingTimeDifferenceWeight;
	double fTotalHidingValueWeight;
	double fLastTotalCompactionValueWithBoundaryWeight;
	double fLastTotalCompactionValueWithScheduledTasksWeight;
	double fLastTotalFinishingTimeDifferenceWeight;
	double fLastTotalHidingValueWeight;
	
	bool fFirstUpdate;
	bool fIsStdScheduler;

	static double sAlpha;
};

#endif
