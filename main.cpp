#include "Common.h"

#include "FPGA.h"
#include "RNG.h"
#include "TaskFactory.h"
#include "Scheduler.h"

int main() {
	const int MAX_ITERATIONS = 10;
	const int MAX_TASKS_PER_ITERATION = 1000;

	const int MIN_INTERTASK_ARRIVAL = 1;
	const int MAX_INTERTASK_ARRIVAL = 150;

	const int MIN_TASK_PER_ARRIVAL = 1;
	const int MAX_TASK_PER_ARRIVAL = 25;

	const int FPGA_WIDTH = 116;
	const int FPGA_HEIGHT = 192;

	Scheduler scheduler;

	double lastValue = 0.0;
	double currentValue = 0.0;

	bool generateData = false;

	ofstream out("mylog.txt");

	for (int i = 1; i <= MAX_ITERATIONS; ++i) {
		cerr << "i = " << i << endl;
		cerr << scheduler << endl;

		FPGA fpga(FPGA_WIDTH, FPGA_HEIGHT);
		int t = 0;
		int totalTasks = 0;
		int scheduledTasks = 0;
		int nextTaskGenerationTime = RNG::RandomInt(MIN_INTERTASK_ARRIVAL, MAX_INTERTASK_ARRIVAL);
		int totalWaiting = 0;
		while (totalTasks < MAX_TASKS_PER_ITERATION) {
			if (nextTaskGenerationTime == 0) {
				int tasksCount = RNG::RandomInt(MIN_TASK_PER_ARRIVAL, MAX_TASK_PER_ARRIVAL);
				for (int j = 1; j <= tasksCount && totalTasks < MAX_TASKS_PER_ITERATION; ++j) {
					auto task = TaskFactory::Create(t);
					++totalTasks;

					bool ok = scheduler.Schedule(task, fpga);

					if (ok) {
						++scheduledTasks;
						totalWaiting += task->GetStartingTime() - task->GetArrivalTime();
					}

					if (generateData)
						out << "'{" << fpga << scheduler << "}'," << endl;
				}

				nextTaskGenerationTime = RNG::RandomInt(MIN_INTERTASK_ARRIVAL, MAX_INTERTASK_ARRIVAL);
			} else {
				--nextTaskGenerationTime;
			}

			fpga.Update(t);

			++t;
		}

		lastValue = currentValue;
		currentValue = scheduledTasks;
		currentValue /= 1 + totalWaiting;

		double avgWaiting = totalWaiting;
		avgWaiting /= totalTasks;

		double missRatio = totalTasks - scheduledTasks;
		missRatio /= totalTasks;

		double deltaValue = currentValue - lastValue;

		cerr << "value = " << currentValue << endl;
		cerr << "total tasks = " << totalTasks << endl;
		cerr << "average waiting time per task = " << avgWaiting << endl;
		cerr << "miss ratio = " << 100 * missRatio << "%" << endl;
		cerr << "DV = " << deltaValue << endl;

		scheduler.UpdateWeights(lastValue, currentValue);
	}

	out.close();
	cin.get();
}
