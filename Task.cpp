#include "Common.h"

#include "Task.h"

Task::Task(int inArrivalTime, int inExecutionTime, int inDeadline, int inWidth, int inHeight) :
fArrivalTime(inArrivalTime),
fExecutionTime(inExecutionTime),
fDeadline(inDeadline),
fWidth(inWidth),
fHeight(inHeight),
fRunningTime(0),
fStartingTime(-1),
fXPos(-1),
fYPos(-1)
{ }

void Task::Update(int inCurrentTime) {
	++fRunningTime;
}

bool Task::IsDone() const {
	return (fRunningTime == fExecutionTime);
}
