#ifndef __STARTING_TIME_MATRIX__
#define __STARTING_TIME_MATRIX__

class StartingTimeMatrix {
public:
	StartingTimeMatrix(int inWidth, int inHeight);
	~StartingTimeMatrix();

	int GetWidth() const { return fWidth; }
	int GetHeight() const { return fHeight; }

	const int& operator() (int inX, int inY) const { return fArray[inX - 1][inY - 1]; }
	int& operator()(int inX, int inY) { return fArray[inX - 1][inY - 1]; }

private:
	int fWidth;
	int fHeight;
	int** fArray;
};

#endif