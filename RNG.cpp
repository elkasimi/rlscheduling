#include "Common.h"

#include "RNG.h"

void RNG::InitSeed() {
	unsigned int s = (unsigned int)time(NULL);
	srand(s);
}

int RNG::RandomInt(int inMinValue, int inMaxValue) {
	int r = rand();
	int w = inMaxValue - inMinValue + 1;
	int res = inMinValue + r % w;

	return res;
}


double RNG::RandomSignedReal() {
	double r = rand();
	r /= RAND_MAX;
	r *= 2.0;
	r -= 1.0;

	return r;
}
