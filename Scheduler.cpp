#include "Common.h"

#include "Scheduler.h"
#include "StartingTimeMatrix.h"
#include "FPGA.h"
#include "Task.h"
#include "RNG.h"

double Scheduler::sAlpha = 1.0;

Scheduler::Scheduler(bool inIsStdScheduler) :
fLastTotalCompactionValueWithBoundaryWeight(0.0),
fLastTotalCompactionValueWithScheduledTasksWeight(0.0),
fLastTotalFinishingTimeDifferenceWeight(0.0),
fLastTotalHidingValueWeight(0.0),
fFirstUpdate(true),
fIsStdScheduler(inIsStdScheduler) {
	if (!inIsStdScheduler) {
		fTotalCompactionValueWithBoundaryWeight = RNG::RandomSignedReal();
		fTotalCompactionValueWithScheduledTasksWeight = RNG::RandomSignedReal();
		fTotalFinishingTimeDifferenceWeight = RNG::RandomSignedReal();
		fTotalHidingValueWeight = RNG::RandomSignedReal();

		NormalizeWeights();
	} else {
		fTotalCompactionValueWithBoundaryWeight = 0.25;
		fTotalCompactionValueWithScheduledTasksWeight = 0.25;
		fTotalFinishingTimeDifferenceWeight = 0.25;
		fTotalHidingValueWeight = 0.25;
	}
}

bool Scheduler::Schedule(Task* inTask, FPGA& inFPGA) {
	StartingTimeMatrix startingTimeMatrix(inFPGA.GetWidth(), inFPGA.GetHeight());
	int smallestStartingTime;
	inFPGA.BuildStartingTimeMatrix(inTask, startingTimeMatrix, smallestStartingTime);

	if (smallestStartingTime > inTask->GetDeadline()) {
		delete inTask;
		return false;
	} else {
		double bestValue = -OO;
		int bestX = -1;
		int bestY = -1;
		for (int x = 1; x <= inFPGA.GetWidth(); ++x)
			for (int y = 1; y <= inFPGA.GetHeight(); ++y) {
				if (startingTimeMatrix(x, y) == smallestStartingTime) {
					double value = 0.0;
					value += inFPGA.GetTotalCompactionValueWithBoundary(inTask, x, y) * fTotalCompactionValueWithBoundaryWeight;
					value += inFPGA.GetTotalCompactionValueWithScheduledTasks(inTask, x, y) * fTotalCompactionValueWithScheduledTasksWeight;
					value += inFPGA.GetTotalFinishingTimeDifference(inTask, x, y) * fTotalFinishingTimeDifferenceWeight;
					value += inFPGA.GetTotalHidingValue(inTask, x, y) * fTotalHidingValueWeight;

					if (bestValue < value) {
						bestValue = value;
						bestX = x;
						bestY = y;
					}
				}
			}

		inTask->SetStartingTime(smallestStartingTime);
		inTask->SetXPos(bestX);
		inTask->SetYPos(bestY);
		inFPGA.Add(inTask);

		return true;
	}
}

static bool IsNullWithEpsilon(double z) {
	return (z < 0.001) && (z > -0.001);
}

double Scheduler::GetUpdatedValue(const double& inLastValue, const double& inDelta) {
	double value = inLastValue + sAlpha * inDelta * pow(inLastValue, 3.0);

	return value;
}

void Scheduler::UpdateWeights(const double& inLastValue, const double& inCurrentValue) {
	if (fIsStdScheduler) {
		return;
	}

	if (!fFirstUpdate) {
		double delta = inCurrentValue - inLastValue;
		
		fTotalCompactionValueWithBoundaryWeight = GetUpdatedValue(fLastTotalCompactionValueWithBoundaryWeight, delta);
		fTotalCompactionValueWithScheduledTasksWeight = GetUpdatedValue(fLastTotalCompactionValueWithScheduledTasksWeight, delta);
		fTotalFinishingTimeDifferenceWeight = GetUpdatedValue(fLastTotalFinishingTimeDifferenceWeight, delta);
		fTotalHidingValueWeight = GetUpdatedValue(fLastTotalHidingValueWeight, delta);

		NormalizeWeights();

	} else {
		fFirstUpdate = false;
	}

	fLastTotalCompactionValueWithBoundaryWeight = fTotalCompactionValueWithBoundaryWeight;
	fLastTotalCompactionValueWithScheduledTasksWeight = fTotalCompactionValueWithScheduledTasksWeight;
	fLastTotalFinishingTimeDifferenceWeight = fTotalFinishingTimeDifferenceWeight;
	fLastTotalHidingValueWeight = fTotalHidingValueWeight;
}


ostream& operator << (ostream& out, const Scheduler& inScheduler) {
	out.setf(ios::fixed);
	out.precision(3);

	out << "\"weights\" : [" << inScheduler.fTotalCompactionValueWithBoundaryWeight << ", "  <<
		inScheduler.fTotalCompactionValueWithScheduledTasksWeight << ", " <<
		inScheduler.fTotalFinishingTimeDifferenceWeight << ", " <<
		inScheduler.fTotalHidingValueWeight << "]";

	return out;
}


void Scheduler::NormalizeWeights() {
	double w = abs(fTotalCompactionValueWithBoundaryWeight) +
		abs(fTotalCompactionValueWithScheduledTasksWeight) +
		abs(fTotalFinishingTimeDifferenceWeight) +
		abs(fTotalHidingValueWeight);


	fTotalCompactionValueWithBoundaryWeight /= w;
	fTotalCompactionValueWithScheduledTasksWeight /= w;
	fTotalFinishingTimeDifferenceWeight /= w;
	fTotalHidingValueWeight /= w;
}
