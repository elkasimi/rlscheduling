#ifndef __RNG__
#define __RNG__

class RNG {
public:
	static void InitSeed();
	static int RandomInt(int inMinValue = 0, int inMaxValue = RAND_MAX);
	static double RandomSignedReal();
};

#endif