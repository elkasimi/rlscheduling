#include "Common.h"

#include "StartingTimeMatrix.h"

StartingTimeMatrix::StartingTimeMatrix(int inWidth, int inHeight) : fWidth(inWidth), fHeight(inHeight) {
	fArray = new int*[inWidth];

	for (int i = 0; i < inWidth; ++i) {
		fArray[i] = new int[inHeight];
	}
}

StartingTimeMatrix::~StartingTimeMatrix() {
	for (int i = 0; i < fWidth; ++i) {
		delete fArray[i];
	}

	delete fArray;
}
