#ifndef __TASK_FACTORY__
#define __TASK_FACTORY__

#include "Task.h"

class TaskFactory {
public:
	static Task* Create(int inTaskArrivalTime);

private:
	static int sMinExecutionTime;
	static int sMaxExecutionTime;
	static int sMinDeadline;
	static int sMaxDeadline;
	static int sMinWidth;
	static int sMaxWidth;
	static int sMinHeight;
	static int sMaxHeight;
};

#endif
