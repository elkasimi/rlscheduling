#include "Common.h"

#include "FPGA.h"
#include "Task.h"
#include "StartingTimeMatrix.h"

FPGA::FPGA(int inWidth, int inHeight) : fWidth(inWidth), fHeight(inHeight) {}

FPGA::~FPGA() {
	for (auto t : fRunningTasks) {
		delete t;
	}

	for (auto t : fWaitingTasks) {
		delete t;
	}
}

void FPGA::Add(Task* inScheduledTask) {
	fWaitingTasks.push_back(inScheduledTask);
}

void FPGA::Update(int inCurrentTime) {
	//update waiting tasks
	for (auto it = fWaitingTasks.begin(); it != fWaitingTasks.end();) {
		auto task = *it;
		if (task->GetStartingTime() == inCurrentTime) {
			fRunningTasks.push_back(task);
			it = fWaitingTasks.erase(it);
		} else {
			++it;
		}
	}

	//update running tasks
	for (auto it = fRunningTasks.begin(); it != fRunningTasks.end();) {
		auto task = *it;
		task->Update(inCurrentTime);
		if (task->IsDone()) {
			it = fRunningTasks.erase(it);
		} else {
			++it;
		}
	}
}

bool FPGA::IsDone() const {
	return (fRunningTasks.empty() && fWaitingTasks.empty());
}

void FPGA::BuildStartingTimeMatrix(const Task* inArrivingTask, StartingTimeMatrix& outStartingTimeMatrix, int& outSmallestStartingTime) const {
	int width = inArrivingTask->GetWidth();
	int height = inArrivingTask->GetHeight();
	int executionTime = inArrivingTask->GetExecutionTime();

	for (int i = 1; i <= fWidth - width + 1; ++i) {
		for (int j = 1; j <= fHeight - height + 1; ++j) {
			outStartingTimeMatrix(i, j) = inArrivingTask->GetArrivalTime();
		}
	}



	for (auto task : fRunningTasks) {
		int x1 = task->GetXPos();
		int y1 = task->GetYPos();
		int x2 = x1 + task->GetWidth();
		int y2 = y1 + task->GetHeight();
		int tf = task->GetFinishingTime();
		for (int i = max(x1 - width + 1, 1); i <= min(x2, fWidth - width + 1); ++i) {
			for (int j = max(y1 - height + 1, 1); j <= min(y2, fHeight - height + 1); ++j) {
				if (outStartingTimeMatrix(i, j) < tf) {
					outStartingTimeMatrix(i, j) = tf;
				}
			}
		}
	}

	for (auto task : fWaitingTasks) {
		int x1 = task->GetXPos();
		int y1 = task->GetYPos();
		int x2 = x1 + task->GetWidth();
		int y2 = y1 + task->GetHeight();
		int ts = task->GetStartingTime();
		int tf = task->GetFinishingTime();
		for (int i = max(x1 - width + 1, 1); i <= min(x2, fWidth - width + 1); ++i) {
			for (int j = max(y1 - height + 1, 1); j <= min(y2, fHeight - height + 1); ++j) {
				if ((outStartingTimeMatrix(i, j) < tf) && (outStartingTimeMatrix(i, j) + executionTime > ts)) {
					outStartingTimeMatrix(i, j) = tf;
				}
			}
		}
	}

	outSmallestStartingTime = OO;

	for (int i = 1; i <= fWidth - width + 1; ++i) {
		for (int j = 1; j <= fHeight - height + 1; ++j) {
			if (outSmallestStartingTime > outStartingTimeMatrix(i, j)) {
				outSmallestStartingTime = outStartingTimeMatrix(i, j);
			}
		}
	}
}

double FPGA::GetTotalCompactionValueWithBoundary(const Task* inArrivingTask, int inX, int inY) const {
	double value = 0.0;

	int x = inX;
	int y = inY;
	int width = inArrivingTask->GetWidth();
	int height = inArrivingTask->GetHeight();
	int executionTime = inArrivingTask->GetExecutionTime();

	if ((x == 1 && y == 1) ||
		(x == 1 && y == fHeight - height + 1) ||
		(x == fWidth - width + 1 && y == fHeight - height + 1) ||
		(x == fWidth - width + 1 && y == 1)) {
		value = (width + height) * executionTime;
	} else if ((x == 1 && y != 1 && y != fHeight - height + 1) ||
		(x == fWidth - width + 1 && y != 1 && y != fHeight - height + 1)) {
		value = height * executionTime;
	} else if ((y == 1 && x != 1 && x != fWidth - width + 1) ||
		(y == fHeight - height + 1 && x != 1 && x != fWidth - width + 1)) {
		value = width * executionTime;
	}

	return value;
}

double FPGA::GetCompactionValue(const Task* inArrivingTask, int inX, int inY, const Task* inScheduledTask) const {
	double value = 0.0;

	struct {
		int x1;
		int x2;
		int y1;
		int y2;
		int w;
		int h;
		int ts;
		int tf;
		int lt;
	} AT = {
		inX,
		inX + inArrivingTask->GetWidth(),
		inY,
		inY + inArrivingTask->GetHeight(),
		inArrivingTask->GetWidth(),
		inArrivingTask->GetHeight(),
		inArrivingTask->GetStartingTime(),
		inArrivingTask->GetFinishingTime(),
		inArrivingTask->GetExecutionTime()
	}, ST = {
		inScheduledTask->GetXPos(),
		inScheduledTask->GetXPos() + inScheduledTask->GetWidth(),
		inScheduledTask->GetYPos(),
		inScheduledTask->GetYPos() + inScheduledTask->GetHeight(),
		inScheduledTask->GetWidth(),
		inScheduledTask->GetHeight(),
		inScheduledTask->GetStartingTime(),
		inScheduledTask->GetFinishingTime(),
		inScheduledTask->GetExecutionTime()
	};

	if ((ST.tf > AT.ts) && !(AT.y1 + AT.h < ST.y1 || AT.y1 > ST.y2 + 1 || AT.x1 + AT.w < ST.x1 || AT.x1 > ST.x2 + 1)) {
		if ((ST.x1 <= AT.x1 && AT.x1 <= ST.x2 && ST.x1 - AT.w + 1 <= AT.x1 && AT.y1 == ST.y1 - AT.h) ||
			(ST.x1 <= AT.x1 && AT.x1 <= ST.x2 && ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 - AT.w + 1 && AT.y1 == ST.y2 + 1)) {
			value = AT.w * min(AT.lt, ST.tf - AT.ts);
		} else if ((ST.y1 <= AT.y1 && AT.y1 <= ST.y2 && ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 - AT.h + 1 && AT.x1 == ST.x1 - AT.w) ||
			(ST.y1 <= AT.y1 && AT.y1 <= ST.y2 && ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 - AT.h + 1 && AT.x1 == ST.x2 + 1)) {
			value = AT.h * min(AT.lt, ST.tf - AT.ts);
		} else if ((ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x1 && ST.x2 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 && AT.y1 == ST.y2 + 1) ||
			(ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x1 && ST.x2 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 && AT.y1 == ST.y1 - AT.h)) {
			value = (ST.x2 - ST.x1 + 1) * min(AT.lt, ST.tf - AT.ts);
		} else if ((ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y1 && ST.y2 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 && AT.x1 == ST.x2 + 1) ||
			(ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y1 && ST.y2 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 && AT.x1 == ST.x1 - AT.w)) {
			value = (ST.y2 - ST.y1 + 1) * min(AT.lt, ST.tf - AT.ts);
		} else if ((ST.x2 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 && AT.y1 == ST.y1 - AT.h) ||
			(ST.x2 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 && AT.y1 == ST.y2 + 1)) {
			value = (ST.x2 - AT.x1 + 1) * min(AT.lt, ST.tf - AT.ts);
		} else if ((ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x1 && AT.y1 == ST.y1 - AT.h) ||
			(ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x1 && AT.y1 == ST.y2 + 1)) {
			value = (AT.x1 + AT.w - ST.x1) * min(AT.lt, ST.tf - AT.ts);
		} else if ((ST.y2 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 && AT.x1 == ST.x2 + 1) ||
			(ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 && AT.x1 == ST.x1 - AT.w)) {
			value = (ST.y2 - AT.y1 + 1) * min(AT.lt, ST.tf - AT.ts);
		} else if ((ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y1 && AT.x1 == ST.x2 + 1) ||
			(ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y1 && AT.x1 == ST.x1 - AT.w)) {
			value = (AT.y1 + AT.h - ST.y1) * min(AT.lt, ST.tf - AT.ts);
		}
	}

	return value;
}

double FPGA::GetTotalCompactionValueWithScheduledTasks(const Task* inArrivingTask, int inX, int inY) const {
	double value = 0.0;

	for (auto task : fWaitingTasks) {
		value += GetCompactionValue(inArrivingTask, inX, inY, task);
	}

	for (auto task : fRunningTasks) {
		value += GetCompactionValue(inArrivingTask, inX, inY, task);
	}

	return value;
}

double FPGA::GetFinishingTimeDifference(const Task* inArrivingTask, int inX, int inY, const Task* inScheduledTask) const {
	double value = 0.0;

	struct {
		int x1;
		int x2;
		int y1;
		int y2;
		int w;
		int h;
		int ts;
		int tf;
		int lt;
	} AT = {
		inX,
		inX + inArrivingTask->GetWidth(),
		inY,
		inY + inArrivingTask->GetHeight(),
		inArrivingTask->GetWidth(),
		inArrivingTask->GetHeight(),
		inArrivingTask->GetStartingTime(),
		inArrivingTask->GetFinishingTime(),
		inArrivingTask->GetExecutionTime()
	}, ST = {
		inScheduledTask->GetXPos(),
		inScheduledTask->GetXPos() + inScheduledTask->GetWidth(),
		inScheduledTask->GetYPos(),
		inScheduledTask->GetYPos() + inScheduledTask->GetHeight(),
		inScheduledTask->GetWidth(),
		inScheduledTask->GetHeight(),
		inScheduledTask->GetStartingTime(),
		inScheduledTask->GetFinishingTime(),
		inScheduledTask->GetExecutionTime()
	};

	if ((ST.tf > AT.ts) && !(AT.y1 + AT.h < ST.y1 || AT.y1 > ST.y2 + 1 || AT.x1 + AT.w < ST.x1 || AT.x1 > ST.x2 + 1)) {
		if ((ST.x1 <= AT.x1 && AT.x1 <= ST.x2 && ST.x1 - AT.w + 1 <= AT.x1 && AT.y1 == ST.y1 - AT.h) ||
			(ST.x1 <= AT.x1 && AT.x1 <= ST.x2 && ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 - AT.w + 1 && AT.y1 == ST.y2 + 1)) {
			value = abs(AT.tf - ST.tf);
		} else if ((ST.y1 <= AT.y1 && AT.y1 <= ST.y2 && ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 - AT.h + 1 && AT.x1 == ST.x1 - AT.w) ||
			(ST.y1 <= AT.y1 && AT.y1 <= ST.y2 && ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 - AT.h + 1 && AT.x1 == ST.x2 + 1)) {
			value = abs(AT.tf - ST.tf);
		} else if ((ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x1 && ST.x2 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 && AT.y1 == ST.y2 + 1) ||
			(ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x1 && ST.x2 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 && AT.y1 == ST.y1 - AT.h)) {
			value = abs(AT.tf - ST.tf);
		} else if ((ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y1 && ST.y2 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 && AT.x1 == ST.x2 + 1) ||
			(ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y1 && ST.y2 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 && AT.x1 == ST.x1 - AT.w)) {
			value = abs(AT.tf - ST.tf);
		} else if ((ST.x2 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 && AT.y1 == ST.y1 - AT.h) ||
			(ST.x2 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x2 && AT.y1 == ST.y2 + 1)) {
			value = abs(AT.tf - ST.tf);
		} else if ((ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x1 && AT.y1 == ST.y1 - AT.h) ||
			(ST.x1 - AT.w + 1 <= AT.x1 && AT.x1 <= ST.x1 && AT.y1 == ST.y2 + 1)) {
			value = abs(AT.tf - ST.tf);
		} else if ((ST.y2 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 && AT.x1 == ST.x2 + 1) ||
			(ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y2 && AT.x1 == ST.x1 - AT.w)) {
			value = abs(AT.tf - ST.tf);
		} else if ((ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y1 && AT.x1 == ST.x2 + 1) ||
			(ST.y1 - AT.h + 1 <= AT.y1 && AT.y1 <= ST.y1 && AT.x1 == ST.x1 - AT.w)) {
			value = abs(AT.tf - ST.tf);
		}
	}

	return value;
}

double FPGA::GetTotalFinishingTimeDifference(const Task* inArrivingTask, int inX, int inY) const {
	double value = 0.0;

	for (auto task : fWaitingTasks) {
		value += GetFinishingTimeDifference(inArrivingTask, inX, inY, task);
	}

	for (auto task : fRunningTasks) {
		value += GetFinishingTimeDifference(inArrivingTask, inX, inY, task);
	}

	return value;
}

double FPGA::GetHidingValue(const Task* inArrivingTask, int inX, int inY, const Task* inScheduledTask) const {
	double value = 0.0;

	struct {
		int x1;
		int x2;
		int y1;
		int y2;
		int w;
		int h;
		int ts;
		int tf;
		int lt;
	} AT = {
		inX,
		inX + inArrivingTask->GetWidth(),
		inY,
		inY + inArrivingTask->GetHeight(),
		inArrivingTask->GetWidth(),
		inArrivingTask->GetHeight(),
		inArrivingTask->GetStartingTime(),
		inArrivingTask->GetFinishingTime(),
		inArrivingTask->GetExecutionTime()
	}, ST = {
		inScheduledTask->GetXPos(),
		inScheduledTask->GetXPos() + inScheduledTask->GetWidth(),
		inScheduledTask->GetYPos(),
		inScheduledTask->GetYPos() + inScheduledTask->GetHeight(),
		inScheduledTask->GetWidth(),
		inScheduledTask->GetHeight(),
		inScheduledTask->GetStartingTime(),
		inScheduledTask->GetFinishingTime(),
		inScheduledTask->GetExecutionTime()
	};

	if ((ST.tf > AT.ts) && !(AT.y1 + AT.h < ST.y1 || AT.y1 > ST.y2 + 1 || AT.x1 + AT.w < ST.x1 || AT.x1 > ST.x2 + 1)) {
	} else if (ST.tf == AT.ts || AT.tf == ST.ts) {
		value = max(min(AT.x1 + AT.w - 1, ST.x2) - max(AT.x1, ST.x1) + 1, 0) * max(min(AT.y1 + AT.h - 1, ST.y2) - max(AT.y1,ST.y1), 0);
	}

	return value;
}

double FPGA::GetTotalHidingValue(const Task* inArrivingTask, int inX, int inY) const {
	double value = 0.0;

	for (auto task : fWaitingTasks) {
		value += GetHidingValue(inArrivingTask, inX, inY, task);
	}

	for (auto task : fRunningTasks) {
		value += GetHidingValue(inArrivingTask, inX, inY, task);
	}

	return value;
}

ostream& operator << (ostream& out, const FPGA& inFPGA) {
	out << "\"waiting\" : [";

	bool firstWaiting = true;
	for (auto task : inFPGA.fWaitingTasks) {
		if (firstWaiting)
			firstWaiting = false;
		else
			out << ", ";
		out << "{\"x\": " << task->GetXPos() <<
			", \"y\" : " << task->GetYPos() <<
			", \"ts\" : " << task->GetStartingTime() <<
			", \"tf\" : " << task->GetFinishingTime() <<
			", \"w\" : " << task->GetWidth() <<
			", \"h\" : " << task->GetHeight() << "}";

	}
	out << "], ";

	out << "\"running\" : [";

	bool firstRunning = true;
	for (auto task : inFPGA.fRunningTasks) {
		if (firstRunning)
			firstRunning = false;
		else
			out << ", ";
		out << "{\"x\": " << task->GetXPos() <<
			", \"y\" : " << task->GetYPos() <<
			", \"ts\" : " << task->GetStartingTime() <<
			", \"tf\" : " << task->GetFinishingTime() <<
			", \"w\" : " << task->GetWidth() <<
			", \"h\" : " << task->GetHeight() << "}";

	}
	out << "],";

	return out;
}
